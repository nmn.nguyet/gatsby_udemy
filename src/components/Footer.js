import React from 'react'
import Navbar from './Navbar'

const Footer = () => {
    return (
        <footer className="page-footer">
            <p>
                &copy; {new Date().getFullYear()}<span>SimpleRecipes. </span>
                Built with {" "} <a href="https://www.gatsbyjs.com/">Gatsby</a>
            </p>
        </footer>
    )
}

export default Footer
