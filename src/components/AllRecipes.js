import React from 'react'
import TagsList from './TagsList'
import RecipesList from './RecipesList'
import {graphql, useStaticQuery} from 'gatsby'

const query = graphql`
  {
    allContentfulRecipe(sort: {fields: title, order: ASC}) {
      nodes {
        id
        title
        cookTime
        prepTime
        content {
          tags
        }
        image {
          gatsbyImageData(layout: CONSTRAINED, placeholder: TRACED_SVG)
        }
      }
    }
  }
`

const AllRecipes = () => {
    //C1
    const data = useStaticQuery(query)
    const recipes = data.allContentfulRecipe.nodes;

    //C2
    // const {allContentfulRecipe:{node: recipes},}
    //  = useStaticQuery(query)

    console.log(recipes);
    return (
        <section className="recipes-container">
            {/* <section className="recipes-container"></section> */}
            <TagsList recipes={recipes}/>
            <RecipesList recipes={recipes}/>
        </section>
    )
}

export default AllRecipes
