import React from 'react'
import {useStaticQuery, graphql} from 'gatsby'

const getData = graphql`
  {
  site{
		info: siteMetadata {
      author
      description
      simpleData
      title
      complexData {
        age
        name
      }
      person {
        age
        name
      }   
    }
  } 
}
`
const FetchData = () => {
  //console.log(useStaticQuery(getData));
  //const data = useStaticQuery(getData);
  const {
    site : {
      info:{title},
    },
  } = useStaticQuery(getData)
  return (
    <div>
      {/* <h1>hello from fetch data</h1> */}
      {/* <h2>Name: {data.site.info.person.name}</h2> */}
      <h2>site title is:{title}</h2>
    </div>
  )
}

export default FetchData

